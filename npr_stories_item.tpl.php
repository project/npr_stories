<?php
/**
 * Theme implementation to display a single NPR story result
 *
 * Available variables:
 * - $display_options: title_only or teaser
 * - $result: raw results of the NPR api call.
 */
 
if($display_options == 'title_only'){
  print l($result['title'], $result['link'],array('html'=>true));
}
else{
  print l($result['title'], $result['link'],array('html'=>true));
  print ' - '. $result['teaser'];
}