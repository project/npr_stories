<?php

class NPR_API {
	private $api_key;
	private $category_array = array(
			'topics' 				=> 	3002,
			'music' 				=>	3018,
			'topics&music'			=>	3218,
			'programs'				=>	3004,
			'bios'					=>	3007,
			'musicartists'			=>	3009,
			'recentmusicartists'	=>	3008,
			'columns'				=>	3003,
			'series'				=>	3006,
      'blogs'         =>  3013,
      'tags'          =>  3024
			);
	public function __construct($key){
		 $this->api_key = $key;
	}
  
	public function query(Array $parameters = array()){
		$query_str = $this->prepareParameters($parameters);
		$request = 'http://api.npr.org/query?'. $query_str;				
		$response = $this->execute($request);
		return $response;		
	}
	
	private function execute($request){
		$request .= '&apiKey='. $this->api_key;
		$session = curl_init($request); 
		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true); 
		$response = curl_exec($session);
		curl_close($session);
		return $response;
	}	
  
	public function getCategories(){
		return $this->category_array;
	}
  
	public function getObject(Array $parameters = array()){
		$parameters['output'] = 'NPRML';
		$result = $this->query($parameters);
		return simplexml_load_string($result);
	}
	
	public function getList($category){
		if(is_int($category)){
			$id = $category;
		}
		else{
			$id = $this->category_array[$category];
		}    
		if(!empty($id)){
			$result = $this->execute('http://api.npr.org/list?id='. $id);
			return simplexml_load_string($result);
		}
    else{
			return 'invalid category';
		}
	}
	
	private function prepareParameters(Array $parameters){
		$new_parameters = array();
		foreach($parameters as $key=>$parameter){
			$new_parameters[] = $key .'='. urlencode($parameter);
		}
		return implode('&',$new_parameters);
	}
}
?>