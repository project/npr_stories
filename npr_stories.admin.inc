<?php

/**
 * @file
 * Settings form.
 */

/**
 * Module settings page.
 */
function npr_stories_admin_settings(&$form_state) {
  $form['npr_stories_api_key'] = array(
    '#title'  =>  t('NPR API key'),
    '#type' =>  'textfield',
    '#default_value' => variable_get("npr_stories_api_key",''),
    '#required' =>  TRUE,
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  $vocabularies_options = array(
    'multi_select_and_tags' => t('Display multi-select and tags types only'),
    'all' => t('Display all vocabulary types'),
    );
  $form['npr_stories_vocabularies'] = array(
    '#type' => 'radios',
    '#title' => t('Vocabularies types to list'),
    '#default_value' => variable_get("npr_stories_vocabularies", 'multi_select_and_tags'),
    '#options' => $vocabularies_options,
    '#required' => FALSE,
  );
  $display_options = array(
    'title_only' => t('Display titles only'),
    'teaser' => t('Display titles and teaser'),
    );
  $form['npr_stories_display_options'] = array(
    '#type' => 'radios',
    '#title' => t('Display Options'),
    '#default_value' => variable_get('npr_stories_display_options', 'title_only'),
    '#options' => $display_options,
    '#required' => FALSE,
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  $order_options = array(
    'relevance' => t('Relevance'),
    'dateAsc' => t('Date (ascending)'),
    'dateDesc' => t('Date (descending)'),
    'assigned' => t('Editor Assigned'),
    'featured' => t('Featured')
    );
  $form['npr_stories_order_options'] = array(
    '#type' => 'radios',
    '#title' => t('Order Options'),
    '#default_value' => variable_get('npr_stories_order_options', 'relevance'),
    '#options' => $order_options,
    '#required' => FALSE,
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  $cache_options = drupal_map_assoc(array(0, 300, 600, 900, 1800, 3600, 7200, 21600, 43200, 86400, 604800, 1209600, 2419200, 4838400, 9676800, 31536000),  'format_interval');
  $cache_options[0] = t('Never');

  $form['npr_stories_cache_options'] = array(
    '#type' => 'select',
    '#title' => t('Cache Options'),
    '#default_value' => variable_get('npr_stories_cache_options', 3600),
    '#options' => $cache_options,
    '#required' => FALSE,
//      '#prefix' => '<div>',
//      '#suffix' => '</div>',
  );

  $form['npr_stories_clear_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear Cache'),
    '#description' => t('Clear NPR Similarterms Block Cache'),
//      '#prefix' => '<div>',
//      '#suffix' => '</div>',
  );
  
  $form['#submit'][] = 'npr_stories_admin_settings_submit';
  return system_settings_form($form);
}

function npr_stories_admin_settings_submit($form, &$form_state) {
  if ($form_state['values']['npr_stories_clear_cache']) {
    cache_clear_all('*', 'cache_npr_stories', TRUE);
    drupal_set_message(t('NPR Similarterms Block Cache is now cleared'));
  }
}


