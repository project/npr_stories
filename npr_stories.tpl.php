<?php

/**
 * Theme implementation to display a list of related content.
 *
 * Available variables:
 * - $display_options: title_only or teaser
 * - $items: formatted items ready to be printed
 * - $results: raw results of the NPR api call.
 */
 
if($items){
  print theme('item_list', $items);
}